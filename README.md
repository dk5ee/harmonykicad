
Kicad Files for https://gitlab.com/dk5ee/harmony

The kicad project is the folder "sidagain"

The folder "eurocad" is a clone of https://github.com/nebs/eurocad - it contains a footprint for DIN Midi connector

The folder "cbm" is a clone of https://github.com/sjgray/cbm-kicad-lib - it contains the SID MOD 6581 chip

The folder "tux.pretty" is a playground for creating multi-layer-graphics on PCBs with up to 5 colours

The file "kicad.md" contains my notes about designing in kicad and creating multi-layer-graphics