# Kicad Notes

Kicad is my go to software for designing printed circuit boards.

Kicad is divided into some components, which interact.

The most important difference to Eagle: the schematic design and the pcb layout are not interacting automaticly.

I assume a two sided board with a solder mask and a silkscreen on each side, and plated through holes.

## Design notes

Designing PCBs in Kicad starts with a plan and a new project.

With Eeschema the schematics are to be designed.

Instead of routing long wires, set "Globals Label", and name them properly.

When placing a "symbol", change its value to the right one.

There are some symbols for power: "GND", "VCC", "VDD". Add a Power Flag to each power symbol.

If a symbol for a special part is missing, a new symbol for that part has to be created, with the symbol editor.

I recommend to add some extra pins, connected to ground, if some mounting holes are desired.

After setting up the schematic, the parts need to be annotated. and then a footprint has to be assigned to every part.

If a footprint for a special part is missing, a new footprint for that part has to be created, with the footprint editor.


after that, the "netlist" has to be exported from eeschema.

with "pcbnew" the pcb can be created and designed

first steps i recommend: edit the default net class and create new net classes (setup -> design rules).

also edit design rules according to recommendations of the PCB company.
set the clearance for small signals to a little more than that, what the pcb manufacturer offers, and adjust the track width.

for high voltage net class, increase the clearance.

for any power net class, i increase the track width, as this reduces voltage drops.

after that, edit the net class memberships.

the steps in pcbnew are:

  * layout the edges of the pcb, on the layer "edge cuts". take care, that the edge is one closed line around the area for the pcb
  * import the net list. any change in the schematic requires an update of the netlist followed by a new import of the netlist in pcbnew, this is the biggest workflow difference to eagle.
  * place the connectors along the edges, and the most complex parts where they might end. useful keyboard shortcuts: "m" for move, "r" for rotate.
  * then place the additional parts like resistors, capacitors etc arround.
  * whenever it suits, some filled zones can be created, one for each side. either make both ground layers, or set one to supply voltage.

some design tips:

  * add dimensions to any part, which interacts physically to the case or anything, like connectors, switches, knobs, leds.
  * place parts with many connections in a way, that the connections between are as short as possible
  * try to place parts only on one side. take into consideration sockets, access to screws, etc
  * make high frequency lines as short as possible, this means clock lines and crystals.
  * if a line has low frequency signals, like less than 1MHz, ignore rounding corners, but place them away from high frequency lines.
  * either try to place all lines on one side, and leave the other side of the pcb filled with ground, or make the design nice by placing only horizontal lines on one side and only vertical lines on the other side, connected by vias

## Graphics notes.

with gimp or whatever, create some bitmap graphics.

for gray scale images, make a halftone graphic, see https://gimpchat.com/viewtopic.php?f=23&t=9266

with image magick:

```
convert h8x8o.png -morphology open diamond:1 h8x8o_open_d1.png
convert h8x8o.png -morphology open diamond:1 h8x8o_open_d2.png
convert h8x8o.png -median 1 h8x8o_med1.png
convert h8x8o.png -median 2 h8x8o_med1.png
convert -size 20x640  gradient: -rotate 90  gradient.png
convert gradient.png   -ordered-dither c7x7b      od_c7x7b.gif
convert gradient.png   -ordered-dither c7x7w      od_c7x7w.gif
```

given a "normal" pcb, there are four layers for graphical designs on each side of the pcb:

* the copper layer "cu"
* the mask layer "mask", which can be placed on plain pcb or on copper
* ignore the paste layer "paste", which will be tinned, only on copper without mask. This is only used for creating SMD stencils.
* the silkscreen layer, which will only be applied to to mask, with or without copper. silkscreen will be misaligned the most, by a fraction of a mm.

this gives the list of layers:

  * plain PCB, in the colour of the pcb
  * only copper
  * mask, on the pcb, mixes with the colour of the PCB
  * mask, on copper layer, more shiny in light
  * silkscreen on mask on plain PCB
  * (silkscreen on mask on copper, with might be slightly elevated)

this gives the following colours:

* dark PCB
* mask on dark PCB
* mask on copper
* copper, which will oxidize or will be tinned

creating the graphics for kicad:

  * first remove all unwanted graphics from footprints by selecting them, click "e", and set to "hide". this will take a while.
  * the dimensions of the graphics have to be defined, like 20mm x 20mm.
  * create bitmaps with 600DPI or higher
  * make "white" the areas which should be silkscreen, copper, tin or mask, black where the layer should be removed
  * import each layer with "bitmap to component converter" and save as with a suitable name in a directory with ".pretty" as suffix.

then comes manual text editing:

* in the layer files for copper and mask replace all F.SilkS with F.Cu and F.Mask
* then combine the data to one single file.

the singe file starts with:

```
(module myLOGO (layer F.Cu)
(at 0 0)
(fp_text reference "combined" (at 0 0) (layer F.SilkS) hide
(effects (font (thickness 0.3)))
)
(fp_text value "myLOGO" (at 0.75 0) (layer F.SilkS) hide
(effects (font (thickness 0.3)))
)
```

and ends with a closing bracket.

from the four layer files, insert all polygons into the new before the closing bracket file:

```
(fp_poly (pts (xy 1.0 1.0) [...] )(layer F.[Cu/F.SilkS/F.Paste/F.Mask) (width  0.010000)
)
```

and save that as mylogo.kicad_mod

for logo on the back side, first mirror the bitmaps vertically, and replace all "F." with "B." in the .kicad_mod files

in kicad pcbnew, add that new library by 'preferences' -> 'manage footprint libraries' -> 'browse libraries' -> select the folder
now the logo can be placed with 'add footprints' -> 'select by browser'

check the logo with 'view' -> '3d viewer'

## exporting gerber files

first, check the design rules with the design rules checker.

then klick on 'file' -> 'plot'

check these layers:

* Top Copper (F.Cu)
* Soldermask (F.Mask)
* Silkscreen (F.SilkS)
* Bottom Copper (B.Cu)
* Soldermask (B.Mask)
* Silkscreen (B.SilkS)
* Board outline (Edge.Cuts)

activate "subtract soldermask from silkscreen"

maybe deactivate "plot footprints values" and "plot footprints references"

select "use protel filename extensions", do not use "extended attributes"
and click on 'plot'

do not forget to click on 'genereate drill file'

in this window deactivate "merge PTH and NPTH", do not use any "X2", and click "drill file"

lastly, check the files with "gerbview"

then put all files into one zip file and send it to PCB lab.


